# VUE Exam Kiwi Last Mile

In the following link you will find 4 videos that will get you up and running with Vue js:

https://wetransfer.com/downloads/57a4204b441c18b579c21f259155378820180903172511/87f0131ab3470201f7a45dc3b7388e2320180903172511/150f63

To do:
- Create a todo list that shows pending tasks.
- Create a way to add new tasks to the list.
- Create a way to delete specific tasks from the list.
- Create a way to mark them as completed.
- When loading the page, it should have the task: 'Finish vue exam'.



Additional notes:
 - This is a frontend test. Try to make the UI look nice. If you need support feel free to use bulma as your css framework. CDN here: https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.css
- If have not completed the test in 2h please stop there as we will review the code done and you will have the opportunity to justify it.
- The test is meant to be serverless. If the page is refreshed data should be restarted by the browser.
- No jQuery allowed.

